import requests
import re
import time
import nltk
from nltk import ne_chunk, pos_tag, word_tokenize
from nltk.tree import Tree
from nltk.corpus import treebank, stopwords
from nltk.stem import PorterStemmer
from nltk.stem import wordnet
from nltk.stem import WordNetLemmatizer
from sklearn.feature_extraction.text import CountVectorizer
from sklearn.metrics.pairwise import cosine_similarity
from sklearn.metrics.pairwise import cosine_similarity
import builtins
from bs4 import BeautifulSoup
from bs4 import Comment
import warc
import re
from nltk.corpus import treebank
from pattern.web import plaintext
import html5lib
import subprocess

KEYNAME = "WARC-TREC-ID"


def find_labels(payload, labels):
    key = None
    for line in payload.splitlines():
        if line.startswith(KEYNAME):
            key = line.split(': ')[1]
            break
    for label, freebase_id in labels.items():
        if key and (label in payload):
            yield key, label, freebase_id

def split_records(stream):
    payload = ''
    for line in stream:
        if line.strip() == "WARC/1.0":
            yield payload
            payload = ''
        else:
            payload += line

def cleanMe(html):
    soup = BeautifulSoup(html, "html.parser")
    [x.extract() for x in soup.find_all('script')]
    [x.extract() for x in soup.find_all('style')]
    [x.extract() for x in soup.find_all('meta')]
    [x.extract() for x in soup.find_all('noscript')]
    [x.extract() for x in soup.find_all(text=lambda text:isinstance(text, Comment))]
    return soup




def search(domain, query):
    url = 'http://%s/freebase/label/_search' % domain
    response = requests.get(url, params={'q': query, 'size':10})
    id_labels = {}
    if response:
        response = response.json()
        for hit in response.get('hits', {}).get('hits', []):
            freebase_max_score = hit.get('_score')
            freebase_label = hit.get('_source', {}).get('label')
            freebase_id = hit.get('_source', {}).get('resource')
            id_labels.setdefault(freebase_id, set()).add( freebase_max_score)
           # id_labels.setdefault(freebase_id, set()).add( freebase_label)
            #id_labels[freebase_id].add( freebase_label)
    return id_labels

def get_final_chunks(text):
    chunked = ne_chunk(pos_tag((my_removestopwords(word_tokenize(text)))))
    final_chunk = []
    current_chunk = []
    for i in chunked:
        if type(i) == Tree:
            current_chunk.append(" ".join([token for token, pos in i.leaves()]))
        elif current_chunk:
            named_entity = " ".join(current_chunk)
            if named_entity not in final_chunk:
                final_chunk.append(named_entity)
                current_chunk = []
        else:
            continue
    return final_chunk

def read_multiplefiles():

    with open('out_final.txt') as myfile:
       text = myfile.read()
       text = text.replace("\n","")
    return text

def my_stemming(tokens):
    pst = PorterStemmer()
    stem = ""
    for word in tokens:
            stem += pst.stem(word)+" "
    StemList = stem.split (' ')
    return StemList

def my_lemmatizer(tokens):
    word_lem = WordNetLemmatizer()
    lemmatize =""
    for words in tokens:
        lemmatize += word_lem.lemmatize(words)+" "
    LemmatizeList = lemmatize.split(' ')
    return LemmatizeList

def my_removestopwords(StemList):
    filtered_word_list = StemList[:]
    for word in StemList:
        if word in stopwords.words('english'):
            filtered_word_list.remove(word)
    return filtered_word_list


def get_final_chunks2(text):
    chunked = ne_chunk(pos_tag((my_removestopwords(word_tokenize(text)))))
    final_chunk = []
    final2_chunk = []
    current_chunk = []
    current_chunk2 = []
    for i in chunked:
        if type(i) == Tree:

            current_chunk.append(" ".join([token for token, pos in i.leaves()]))
            string_chunk = " ".join(str(x) for x in current_chunk)
            current_chunk2.append(string_chunk)
            current_chunk2.append(i.label())
        elif current_chunk2:

            if current_chunk2[0] not in final_chunk:
                final_chunk.append((current_chunk2[0],current_chunk2[1]))
                current_chunk = []
                current_chunk2 = []

        else:
            continue
    return final_chunk



def top5(domain,query):
    final_list = list()
    final_list2 = list()
    for entity,maxscore in search(domain,query).items():
        maxscore_list= list(maxscore)
        maximum_score = max(maxscore_list)
        final_list.append((entity,maximum_score))
    sortedList = sorted(final_list, key=lambda x: x[1], reverse=True)[:10]
    for i in sortedList:
        my_string = str(i[0])
        final_freebase_id = my_string.split("/",2)[2]
        final_list2.append(final_freebase_id)
    return final_list2

def sparql1(domain, query):
    url = 'http://%s/sparql' % domain
    response = requests.post(url, data={'print': True, 'query': query})
    if response:
        try:
            response = response.json()
            url_list = list()
            for result in response['results']['bindings']:
                url = result['o']['value']
                url = url.rsplit('/', 1)[-1]
                url_list.append(url)
            return url_list
        except Exception as e:
            print(reponse)
            raise e
            
def entitymap(final_subject,expanded_query1,domain):
    entity_type=expanded_query1[1]
    matched_entity_list = list()
    entity_mapping ={"LOCATION":"location","LOC":"location","PERSON":"person","GPE":"location","ORGANIZATION":"Organization","GSP":"Organization","FACILITY":"facility"}

    for i in range(len(final_subject)):
        fs = str("<"+"http://rdf.freebase.com/ns/m."+final_subject[i]+">")
        query = "select * where {"+fs+" <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> ?o} limit 10"
        url_list = sparql1(domain,query)
        for url in url_list:
            if entity_mapping[entity_type] in url:
                matched_entity_list.append(final_subject[i])
    return matched_entity_list

def findsimilarity(text1,text2):
    text1 = [text1]
    text2 = [text2]
    vectorizer = CountVectorizer()
    vectorizer.fit(text1)
    text = text1+text2
    vectorizer.fit(text)
    vector = vectorizer.transform(text)
    similarity = cosine_similarity(vector)
    sim = similarity[0][1]
    return sim


def sparql2(domain,query,mention):
    url = 'http://%s/sparql' % domain
    response = requests.post(url, data={'print': True, 'query': query})
    if response:
        try:
            response= response.json()
            for result in response['results']['bindings']:
                val = result['abstract']['value']
                m = re.search('@en', val)
                if(m):
                    sim=findsimilarity(val,mention)
                    print("similarity is")
                    print(sim)
                    return sim
            return 0
        except Exception as e:
            raise e




def cosine_similarity1(final_list,expanded_query,record,domain):
    mention = expanded_query[0]
    sortedList= list()
    abstract_return_list = list()
    cosine_similarity_list = list()
    for i in range(len(final_list)-1):
        fs = str("<"+"http://rdf.freebase.com/ns/m."+final_list[i]+">")
        query ="select distinct ?abstract where {  \
  ?s <http://www.w3.org/2002/07/owl#sameAs>" +fs+"   .  \
  ?s <http://www.w3.org/2002/07/owl#sameAs> ?o . \
  ?o <http://dbpedia.org/ontology/abstract> ?abstract . \
}"
        #output1 =sparql2(domain,query,mention)
        output1 = sparql2(domain,query,record)
        cosine_similarity_list.append((output1,final_list[i]))
    try:
        sortedList = sorted(cosine_similarity_list, key=lambda x: x[0], reverse=True)[:1]
    except Exception as e:
        return
    return sortedList

def writetofile(filename,query,freebase_id):
    file= open(filename,"a")
    file.write(query+"\t"+"/m/"+str(freebase_id)+"\n")
    file.close()

def writetofile2(filename,my_string):
    file= open(filename,"a")
    file.write(my_string)
    file.close()


            
if __name__ == '__main__':
    import sys
    try:
        _, DOMAIN_ELASTICSEARCH,DOMAIN_SPARQL,INPUT = sys.argv
    except Exception as e:
        print('Usage: bash run2.sh INPUTFILE')
        sys.exit(0)

    chunk_list =list()
    chunk_list_small = list()
    open("out_final.txt",'w')
    open("freebase.txt",'w')
    f = warc.open(INPUT)
    print("input is")
    print(INPUT)
    count = 0
    for record in f:
        count = count+1
        print("count is")
        print(count)
        if record.header['Warc-type'] == 'response':
            data =  record.payload.read()
            header ,text = data.split(b'\r\n\r\n',1)
            soup =cleanMe(text)
            content = soup.get_text().encode('ascii', 'ignore').decode('ascii')
            content = ''.join([i for i in content if not i.isdigit()])
            content = re.sub(r"[+':;,.&{}()@$%^*-]", '', content)
            file_object = open('out_final.txt','w')
            file_object.write(content)
            file_object.close()

            sample = read_multiplefiles()
            #Performing NER
            chunk_list = get_final_chunks2(sample)
            #Extract top5 entities 
            for i in range(0,len(chunk_list)-1):
                final_subject = top5(DOMAIN_ELASTICSEARCH,chunk_list[i][0])
                #Compare the type of the entity mention with the type of the candidate entities
                if(len(final_subject)>0):
                    refined_final_subject = entitymap(final_subject,chunk_list[i],DOMAIN_SPARQL)
                    #Compare the cosine similarity between the content of the record and the abstract of the candidate entity 
                    if(len(refined_final_subject)>0):
                        final_freebaseid = cosine_similarity1(refined_final_subject,chunk_list[i],content,DOMAIN_SPARQL)
                        if(len(final_freebaseid) > 0):
                           writetofile("freebase.txt",chunk_list[i][0],final_freebaseid[0][1])
    
    

                                                                                         
            
            
            
            
            
            
            
            
            
            
            
            
            
            
            

































































































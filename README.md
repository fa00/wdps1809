### WDPS1809

Web Data Processing System Assignment - Group 9 - 2018/2019
Vibha Rao 
Saliha Tabbassum
Abhinav Shankar

Large Scale Entity Linking - Assignment 

## Basic Idea 

Extract text from HTML pages in the WARC file
Generated entities using NLTK 
Based on string similarity of the entity mention and the freebase entities, that is returned by the elastic search we retain the top10 candidate entities.
We take the type of the entity mention and compare it with the type of each of the candidate entities. When only there is a match we retain the candidate entity and filter the rest.
Finally, we find the cosine similarity between the entity mention and the abstract of the candidate entities. And selected the candidate entity with the highest cosine similarity

## Main dependencies

--- Python packages --- The following python packages have to be installed in order to use the program: scipy, sklearn, bs4, nltk, warc, subprocess,  html5lib 

## How to run the code

1.	Get the project (git clone https://fa00@bitbucket.org/fa00/wdps1809.git)
2.	bash run.sh INPUTFILE (preferably the INPUTFILE must be in the same directory as the code) runs the file wdps1809.py, which is our first way of disambiguation based on taking the top10 candidates and then performing a cosine similarity between the entity mention and the abstract of the candidate entity. 
3.	bash run2.sh INPUTFILE runs the wdps1809-2.py which is our second way of disambiguation where we take the top5 candidate entities and take the type of the entity mention and compare it with the type of each of the candidate entities, then only if the type matches perform a cosine similarity between the entity mention’s context and the English abstract of the candidate key.
4.	Bash run3.sh INPUTFILE runs the wdps1809-3.py which is our third way of disambiguation where we take the top5 candidate entities and take the type of the entity mention and compare it with the type of each of the candidate entities, but unlike the last approach we calculate just the cosine similarity between the entity mention and the English abstracts of the candidate entities .
5.	The freebase.txt file consists of entity mentions with its predicted freebase ids.
6.	In order to get the F1 score open the score.sh file and replace sample.warc.gz with the name of the hidden INPUTFILE (The output consisting of clues, entity, freebase ids would be generated in sample_predictions.tsv). Also replace the name of the gold predictions file in place of sample.annotations.tsv. 
7.	run this command: bash score.sh

Depending on the size of the file if a timeout takes place , the code is designed such that the results till the time will be stored in freebase.txt

